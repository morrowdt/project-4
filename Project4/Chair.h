#pragma once

#include "OGL3DCompositeObject.h"

class Cuboid;

class Chair :
	public OGL3DCompositeObject
{
protected:
	Cuboid *topLeftLeg;
	Cuboid *topRightLeg;
	Cuboid *bottomLeftLeg;
	Cuboid *bottomRightLeg;
	Cuboid *seat;
	Cuboid *back;
	int scale;

public:
	Chair(const string& name, int scale);
	virtual ~Chair();

	void setShaderProgram(GLuint shaderProgram);

	//void update(float elapsedSeconds);

	void render();
};

