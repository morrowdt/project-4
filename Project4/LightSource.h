#pragma once
#ifndef LOCAL_LIGHT_SOURCE
#define LOCAL_LIGHT_SOURCE

#include "MathUtil.h"

class LightSource
{
protected:
	Vector3f position;
	float intensity;
	float originalIntensity;
	float activationDistance;

public:
	LightSource(float originalIntensity, float activationDistance);
	virtual ~LightSource();

	void setPosition(float x, float y, float z) { this->position = { x, y, z }; }
	
	const Vector3f& getPosition() { return this->position; }

	void setIntensity(float intensity) { this->intensity = intensity; }

	float getIntensity() { return this->intensity; }

	virtual void update(float elapsedSeconds, float* xyz);
};

#endif

