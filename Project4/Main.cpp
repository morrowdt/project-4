#pragma comment(lib, "glew32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glutilD.lib")

#include "GameEngine.h"
#include "WindowsConsoleLogger.h"
#include "GameWindow.h"
#include "OGLVertexShader.h"
#include "OGLFragmentShader.h"
#include "OGLShaderProgram.h"
#include "OGLShaderManager.h"
#include "TextFileReader.h"
#include "OGLGraphicsSystem.h"
#include "CoreSystem.h"
#include "GameWorld.h"
#include "GameObjectManager.h"
#include "OGLSphericalCamera.h"
#include "OGLViewingFrustum.h"
#include "StockObjectLoader.h"
#include "PCInputSystem.h"
#include "WindowsTimer.h"
#include "OGLFirstPersonCamera.h"
#include "LightSource.h"
#include "TheGame.h"

#include <Windows.h>
#include <iostream>
#include <fstream>
#include <string>
using std::string;
using namespace std;

/**
* Method Name: ReadConfig <br>
* Method Purpose: Reads the configuration for a windows from a file <br>
*
* <hr>
* Date created: 8/31/2015 <br>
*
* <hr>
*   @param  configFile - name of the configuration file
*/
void ReadConfig(string configFile, string &title, int &width, int &height, bool &max)
{
	string configData;		// storage for parsing config data
	ifstream fin;			// Input file stream
	fin.open(configFile);
	if (fin.fail())
	{
		cout << "\nThere was error opening the file." << endl;
		exit(1);
	}

	while (!fin.eof())		// While there is data to be read...
	{
		getline(fin, configData); // Read in a line from the configuration file
		if (configData.find("title:") != string::npos)			// If the line is a title, set the title
			title = configData.substr(6, string::npos);
		if (configData.find("width:") != string::npos)			// If the line is the width, convert and set the width
			width = stoi(configData.substr(6, string::npos));
		if (configData.find("height:") != string::npos)			// If the line is the height, convert and set the height
			height = stoi(configData.substr(7, string::npos));
		if (configData.find("max:") != string::npos)			// If the line is maximize condition, set max
		{
			configData = configData.substr(4, string::npos);
			if (configData == "true")
			{
				max = true;
			}
			else
			{
				max = false;
			}
		}
	}
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	string title;
	int width, height;
	bool max;

	ReadConfig("window.config", title, width, height, max);

	wstring wTitle(title.begin(), title.end()); // Convert string to wstring

	GameEngine gameEngine(
		new WindowsConsoleLogger(), 
		new TheGame(),
		new CoreSystem(
			new TextFileReader()), 
		new OGLGraphicsSystem(
			new OGLShaderManager(), 
			new GameWorld(
				new GameObjectManager(),
				new OGLFirstPersonCamera()),
			new OGLViewingFrustum()),
		new GameWindow(
			wTitle, 
			width, 
			height, max),
		new PCInputSystem(),
		new WindowsTimer()
	);
	gameEngine.loadShaders();
	gameEngine.initializeWindowAndGraphics();
	StockObjectLoader loader;
	gameEngine.setupGame(&loader);
	gameEngine.run();

	return 0;
}
