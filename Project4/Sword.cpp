#include "Sword.h"
#include "Cuboid.h"

Sword::Sword(const string& name) : OGL3DCompositeObject(name)
{
	this->grip = new Cuboid("grip", 0.1, 0.1, 0.4, { 0.443f, 0.274f, 0.184f });
	this->crossguard = new Cuboid("crossguard", 0.2, 0.1, 0.1, { 0.443f, 0.274f, 0.184f });
	this->blade = new Cuboid("blade", 0.05, 0.05, 2, { 0.568f, 0.568f, 0.568f });
	this->blade->material.shininess = 100;
	this->blade->material.specular = { 1, 1, 1, 1 };
}

Sword::~Sword()
{
	delete this->grip;
	delete this->crossguard;
	delete this->blade;
}

void Sword::setShaderProgram(GLuint shaderProgram)
{
	this->shaderProgram = shaderProgram;
	this->grip->setShaderProgram(this->shaderProgram);
	this->crossguard->setShaderProgram(this->shaderProgram);
	this->blade->setShaderProgram(this->shaderProgram);
}

void Sword::update(float elapsedSeconds)
{
	OGL3DCompositeObject::update(elapsedSeconds);
}

void Sword::render()
{
	this->grip->referenceFrame = this->referenceFrame;
	this->frameStack.setBaseFrame(this->grip->referenceFrame);
	this->grip->render();

	this->frameStack.push();
	{
		this->frameStack.translate(0, 0.25f, 0);

		this->crossguard->referenceFrame = this->frameStack.top();
		this->crossguard->render();

		this->frameStack.push();
		{
			this->frameStack.translate(0, 1.05f, 0);

			this->blade->referenceFrame = this->frameStack.top();
			this->blade->render();
		}
		this->frameStack.pop();
	}
	this->frameStack.pop();
}
