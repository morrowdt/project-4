#include "StockObjectLoader.h"
#include "OGL3DObject.h"
#include "ObjectGenerator.h"
#include "GameObjectManager.h"
#include "Cuboid.h"
#include "Turret.h"
#include "Axis.h"
#include "Arm.h"
#include "Sword.h"
#include "Chair.h"

#include <gl\glew.h>
#include <cstdlib>
#include <ctime>

StockObjectLoader::StockObjectLoader()
{
	srand((unsigned int)time(NULL));
}

StockObjectLoader::~StockObjectLoader()
{
}

void StockObjectLoader::loadObjects(GameObjectManager *gameObjectManager)
{
	OGLObject *object;
	VBOObject* vboObject;

	//object = new Axis("Axes", 3.0f);
	//gameObjectManager->addObject("Axes", object);

	object = new Chair("Chair", 2);
	gameObjectManager->addObject("Chair", object);

	object = new OGL3DObject("Ground");
	object->setIndexedArrayType();
	ElementArray arr = ObjectGenerator::generateFlatSurface(10, 10, 20, 30, { 0.0f, 0.4f, 0.0f, 1.0f });
	object->setVertexData(arr.vertexData);
	object->setIndexData(arr.indexData);
	vboObject = OGLObject::createVBOObject(
		"triangles", arr.vertexData, arr.indexData, GL_TRIANGLES);
	object->addVBOObject(vboObject);
	gameObjectManager->addObject("Ground", object);

	object = new OGL3DObject("Left Wall");
	object->setIndexedArrayType();
	arr = ObjectGenerator::generateFlatSurface(10, 10, 10, 30, { 1.0f, 0.0f, 0.0f, 1.0f });
	object->setVertexData(arr.vertexData);
	object->setIndexData(arr.indexData);
	vboObject = OGLObject::createVBOObject(
		"triangles", arr.vertexData, arr.indexData, GL_TRIANGLES);
	object->addVBOObject(vboObject);
	object->referenceFrame.move(glm::vec3(-1, 0, 0), 10);
	object->referenceFrame.move(glm::vec3(0, 1, 0), 5);
	object->referenceFrame.rotateZ(-90);
	gameObjectManager->addObject("Left Wall", object);

	object = new OGL3DObject("Back Wall");
	object->setIndexedArrayType();
	arr = ObjectGenerator::generateFlatSurface(10, 10, 20, 10, { 1.0f, 0.0f, 0.0f, 1.0f });
	object->setVertexData(arr.vertexData);
	object->setIndexData(arr.indexData);
	vboObject = OGLObject::createVBOObject(
		"triangles", arr.vertexData, arr.indexData, GL_TRIANGLES);
	object->addVBOObject(vboObject);
	object->referenceFrame.move(glm::vec3(0, 0, -1), 15);
	object->referenceFrame.move(glm::vec3(0, 1, 0), 5);
	object->referenceFrame.rotateX(90);
	gameObjectManager->addObject("Back Wall", object);

	object = new OGL3DObject("Front Wall");
	object->setIndexedArrayType();
	arr = ObjectGenerator::generateFlatSurface(10, 10, 20, 10, { 1.0f, 0.0f, 0.0f, 1.0f });
	object->setVertexData(arr.vertexData);
	object->setIndexData(arr.indexData);
	vboObject = OGLObject::createVBOObject(
		"triangles", arr.vertexData, arr.indexData, GL_TRIANGLES);
	object->addVBOObject(vboObject);
	object->referenceFrame.move(glm::vec3(0, 0, 1), 15);
	object->referenceFrame.move(glm::vec3(0, 1, 0), 5);
	object->referenceFrame.rotateX(90);
	object->referenceFrame.rotateZ(180);
	gameObjectManager->addObject("Front Wall", object);

	object = new OGL3DObject("Right Wall");
	object->setIndexedArrayType();
	arr = ObjectGenerator::generateFlatSurface(10, 10, 10, 30, { 1.0f, 0.0f, 0.0f, 1.0f });
	object->setVertexData(arr.vertexData);
	object->setIndexData(arr.indexData);
	vboObject = OGLObject::createVBOObject(
		"triangles", arr.vertexData, arr.indexData, GL_TRIANGLES);
	object->addVBOObject(vboObject);
	object->referenceFrame.move(glm::vec3(1, 0, 0), 10);
	object->referenceFrame.move(glm::vec3(0, 1, 0), 5);
	object->referenceFrame.rotateZ(90);
	gameObjectManager->addObject("Right Wall", object);

	object = new OGL3DObject("Roof");
	object->material.shininess = 100;
	object->material.specular = { 1, 1, 1, 1 };
	object->setIndexedArrayType();
	arr = ObjectGenerator::generateFlatSurface(10, 10, 20, 30, { 1.0f, 1.0f, 0.0f, 1.0f });
	object->setVertexData(arr.vertexData);
	object->setIndexData(arr.indexData);
	vboObject = OGLObject::createVBOObject(
		"triangles", arr.vertexData, arr.indexData, GL_TRIANGLES);
	object->addVBOObject(vboObject);
	object->referenceFrame.move(glm::vec3(0, 1, 0), 10);
	object->referenceFrame.rotateX(180);
	gameObjectManager->addObject("Roof", object);

	object = new Cuboid("FireBase", 2.75, 2.75, 0.1, { 0.25, 0.25, 0.25, 1 });
	object->referenceFrame.translate(0.25, 0.05, -10);
	gameObjectManager->addObject("FireBase", object);

	object = new OGL3DObject("Lamp");
	object->setIndexedArrayType();
	arr = ObjectGenerator::generatePyramidIndexedArray(1, 1, 1, {0.75, 0.75, 0.75, 1});
	object->setVertexData(arr.vertexData);
	object->setIndexData(arr.indexData);
	vboObject = OGLObject::createVBOObject(
		"triangles", arr.vertexData, arr.indexData, GL_TRIANGLES);
	object->addVBOObject(vboObject);
	object->referenceFrame.move(glm::vec3(0, 0, 1), 9.5);
	object->referenceFrame.move(glm::vec3(0, 1, 0), 9.5);
	gameObjectManager->addObject("Lamp", object);

	/*object = new Turret("Turret");
	gameObjectManager->addObject("Turret", object);*/

	/*Sword* sword = new Sword("Sword");
	gameObjectManager->addObject("Sword", sword);

	Arm* arm = new Arm("Left Arm");
	arm->giveSword(sword);
	gameObjectManager->addObject("Left Arm", arm);*/

	object = new OGL3DObject("WallWithDoorFront");
	object->setIndexedArrayType();
	arr = ObjectGenerator::generateWallWithDoor(20, 10, { 0.0f, 0.0f, 1.0f, 1.0f });
	object->setVertexData(arr.vertexData);
	object->setIndexData(arr.indexData);
	vboObject = OGLObject::createVBOObject(
		"triangles", arr.vertexData, arr.indexData, GL_TRIANGLES);
	object->addVBOObject(vboObject);
	object->referenceFrame.move(glm::vec3(0, 1, 0), 5);
	//object->referenceFrame.rotateX(180);
	gameObjectManager->addObject("WallWithDoorFront", object);

	object = new OGL3DObject("WallWithDoorBack");
	object->setIndexedArrayType();
	arr = ObjectGenerator::generateWallWithDoor(20, 10, { 0.0f, 0.0f, 1.0f, 1.0f });
	object->setVertexData(arr.vertexData);
	object->setIndexData(arr.indexData);
	vboObject = OGLObject::createVBOObject(
		"triangles", arr.vertexData, arr.indexData, GL_TRIANGLES);
	object->addVBOObject(vboObject);
	object->referenceFrame.move(glm::vec3(0, 1, 0), 5);
	object->referenceFrame.rotateY(180);
	gameObjectManager->addObject("WallWithDoorBack", object);
	
	object = new OGL3DObject("FireBig1");
	object->material.ambientIntensity = 1.0f;
	object->setIndexedArrayType();
	arr = ObjectGenerator::generateFireBig();
	object->setVertexData(arr.vertexData);
	object->setIndexData(arr.indexData);
	vboObject = OGLObject::createVBOObject(
		"triangles", arr.vertexData, arr.indexData, GL_TRIANGLES);
	object->addVBOObject(vboObject);
	object->referenceFrame.move(glm::vec3(0, 0, 1), -10.0);
	object->referenceFrame.move(glm::vec3(0, 1, 0), 1.5);
	object->referenceFrame.scale(0.5f);
	gameObjectManager->addObject("FireBig1", object);

	object = new OGL3DObject("FireBig2");
	object->material.ambientIntensity = 1.0f;
	object->setIndexedArrayType();
	arr = ObjectGenerator::generateFireBig();
	object->setVertexData(arr.vertexData);
	object->setIndexData(arr.indexData);
	vboObject = OGLObject::createVBOObject(
		"triangles", arr.vertexData, arr.indexData, GL_TRIANGLES);
	object->addVBOObject(vboObject);
	object->referenceFrame.move(glm::vec3(0, 0, 1), -10.0);
	object->referenceFrame.move(glm::vec3(0, 1, 0), 1.5);
	object->referenceFrame.move(glm::vec3(1, 0, 0), 0.5);
	object->referenceFrame.rotateY(180);
	object->referenceFrame.scale(0.5f);
	gameObjectManager->addObject("FireBig2", object);

	object = new OGL3DObject("FireMed1");
	object->material.ambientIntensity = 1.0f;
	object->setIndexedArrayType();
	arr = ObjectGenerator::generateFireMedium();
	object->setVertexData(arr.vertexData);
	object->setIndexData(arr.indexData);
	vboObject = OGLObject::createVBOObject(
		"triangles", arr.vertexData, arr.indexData, GL_TRIANGLES);
	object->addVBOObject(vboObject);
	object->referenceFrame.move(glm::vec3(0, 0, 1), -10.0);
	object->referenceFrame.move(glm::vec3(0, 1, 0), 1.5);
	object->referenceFrame.move(glm::vec3(1, 0, 0), 0.25);
	object->referenceFrame.move(glm::vec3(0, 0, 1), 0.25);
	object->referenceFrame.rotateY(90);
	object->referenceFrame.scale(0.5f);
	gameObjectManager->addObject("FireMed1", object);

	object = new OGL3DObject("FireMed2");
	object->material.ambientIntensity = 1.0f;
	object->setIndexedArrayType();
	arr = ObjectGenerator::generateFireMedium();
	object->setVertexData(arr.vertexData);
	object->setIndexData(arr.indexData);
	vboObject = OGLObject::createVBOObject(
		"triangles", arr.vertexData, arr.indexData, GL_TRIANGLES);
	object->addVBOObject(vboObject);
	object->referenceFrame.move(glm::vec3(0, 0, 1), -10.0);
	object->referenceFrame.move(glm::vec3(0, 1, 0), 1.5);
	object->referenceFrame.move(glm::vec3(1, 0, 0), 0.25);
	object->referenceFrame.move(glm::vec3(0, 0, 1), -0.25);
	object->referenceFrame.rotateY(-90);
	object->referenceFrame.scale(0.5f);
	gameObjectManager->addObject("FireMed2", object);

	//object = new OGL3DObject("FireSmall1_1");
	////object->material.ambientIntensity = 1.0f;
	//object->setIndexedArrayType();
	//arr = ObjectGenerator::generateFireSmall();
	//object->setVertexData(arr.vertexData);
	//object->setIndexData(arr.indexData);
	//vboObject = OGLObject::createVBOObject(
	//	"triangles", arr.vertexData, arr.indexData, GL_TRIANGLES);
	//object->addVBOObject(vboObject);
	//object->referenceFrame.move(glm::vec3(0, 1, 0), 1.5);
	//object->referenceFrame.move(glm::vec3(1, 0, 0), 0.125);
	//object->referenceFrame.move(glm::vec3(0, 0, 1), 0.125);
	//object->referenceFrame.rotateY(45);
	//object->referenceFrame.scale(0.5f);
	//gameObjectManager->addObject("FireSmall1_1", object);

	//object = new OGL3DObject("FireSmall1_2");
	////object->material.ambientIntensity = 1.0f;
	//object->setIndexedArrayType();
	//arr = ObjectGenerator::generateFireSmall();
	//object->setVertexData(arr.vertexData);
	//object->setIndexData(arr.indexData);
	//vboObject = OGLObject::createVBOObject(
	//	"triangles", arr.vertexData, arr.indexData, GL_TRIANGLES);
	//object->addVBOObject(vboObject);
	//
	//object->referenceFrame.move(glm::vec3(0, 1, 0), 1.5);
	//object->referenceFrame.move(glm::vec3(1, 0, 0), 0.25);
	//object->referenceFrame.move(glm::vec3(0, 0, 1), -0.25);
	//object->referenceFrame.move(glm::vec3(1, 0, 0), 0.125);
	//object->referenceFrame.move(glm::vec3(0, 0, 1), 0.125);

	//object->referenceFrame.rotateY(-135);
	//
	//object->referenceFrame.scale(0.5f);
	//gameObjectManager->addObject("FireSmall1_2", object);

	//object = new Axis("Axes", 3.0f);
	//gameObjectManager->addObject("Axes", object);



	//object = new OGL3DObject("FireSmall2_1");
	////object->material.ambientIntensity = 1.0f;
	//object->setIndexedArrayType();
	//arr = ObjectGenerator::generateFireSmall();
	//object->setVertexData(arr.vertexData);
	//object->setIndexData(arr.indexData);
	//vboObject = OGLObject::createVBOObject(
	//	"triangles", arr.vertexData, arr.indexData, GL_TRIANGLES);
	//object->addVBOObject(vboObject);
	//object->referenceFrame.move(glm::vec3(0, 1, 0), 1.5);
	//object->referenceFrame.move(glm::vec3(1, 0, 0), -0.125);
	//object->referenceFrame.move(glm::vec3(0, 0, 1), -0.125);
	//object->referenceFrame.move(glm::vec3(1, 0, 0), 0.125);
	//object->referenceFrame.move(glm::vec3(0, 0, 1), -0.125);
	//object->referenceFrame.move(glm::vec3(1, 0, 0), 0.125);
	//object->referenceFrame.rotateY(-45);
	//object->referenceFrame.scale(0.5f);
	//gameObjectManager->addObject("FireSmall2_1", object);

	//object = new OGL3DObject("FireSmall2_2");
	////object->material.ambientIntensity = 1.0f;
	//object->setIndexedArrayType();
	//arr = ObjectGenerator::generateFireSmall();
	//object->setVertexData(arr.vertexData);
	//object->setIndexData(arr.indexData);
	//vboObject = OGLObject::createVBOObject(
	//	"triangles", arr.vertexData, arr.indexData, GL_TRIANGLES);
	//object->addVBOObject(vboObject);
	//object->referenceFrame.move(glm::vec3(0, 1, 0), 1.5);
	//object->referenceFrame.move(glm::vec3(1, 0, 0), 0.25);
	//object->referenceFrame.move(glm::vec3(0, 0, 1), 0.25);
	//object->referenceFrame.rotateY(135);
	//object->referenceFrame.scale(0.5f);
	//gameObjectManager->addObject("FireSmall2_2", object);
	//
}
