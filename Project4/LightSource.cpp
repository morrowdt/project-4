#include "LightSource.h"
#include "math.h"

LightSource::LightSource(float originalIntensity, float activationDistance) : position({ 0, 0, 0 }), intensity(0.f)
{
	this->originalIntensity = originalIntensity;
	this->activationDistance = activationDistance;
	if (activationDistance == -1)
	{
		this->setIntensity(originalIntensity);
	}
}

LightSource::~LightSource()
{
}

void LightSource::update(float elapsedSeconds, float* xyz)
{
	if (this->activationDistance != -1)
	{
		float distance = sqrtf(
			powf(this->getPosition().v1 - xyz[0], 2.0f) +
			powf(this->getPosition().v2 - xyz[1], 2.0f) +
			powf(this->getPosition().v3 - xyz[2], 2.0f));

		if (distance >= this->activationDistance)
		{
			this->setIntensity(0.0f);
		}
		else
		{
			this->setIntensity(this->originalIntensity);
		}
	}
}
