#include "Chair.h"
#include "Cuboid.h"


Chair::Chair(const string& name, int scale) : OGL3DCompositeObject(name)
{
	float legWidth = 0.1f * scale;
	float legHeight = 1.0f * scale;
	RGBA color = { 0.5f, 0.25f, 0.25f };

	this->topLeftLeg = new Cuboid("topLeftLeg", legWidth, legWidth, legHeight, color);
	this->topRightLeg = new Cuboid("topRightLeg", legWidth, legWidth, legHeight, color);
	this->bottomLeftLeg = new Cuboid("bottomLeftLeg", legWidth, legWidth, legHeight, color);
	this->bottomRightLeg = new Cuboid("bottomRightLeg", legWidth, legWidth, legHeight, color);
	this->seat = new Cuboid("seat", legHeight, legHeight, legWidth, color);
	this->back = new Cuboid("back", legHeight, legHeight, legWidth, color);
	this->scale = scale;
}


Chair::~Chair()
{
	delete this->topLeftLeg;
	delete this->topRightLeg;
	delete this->bottomLeftLeg;
	delete this->bottomRightLeg;
	delete this->seat;
	delete this->back;
}

void Chair::setShaderProgram(GLuint shaderProgram)
{
	this->shaderProgram = shaderProgram;
	this->topLeftLeg->setShaderProgram(this->shaderProgram);
	this->topRightLeg->setShaderProgram(this->shaderProgram);
	this->bottomLeftLeg->setShaderProgram(this->shaderProgram);
	this->bottomRightLeg->setShaderProgram(this->shaderProgram);
	this->seat->setShaderProgram(this->shaderProgram);
	this->back->setShaderProgram(this->shaderProgram);
}

void Chair::render()
{
	float moveHeight1 = 0.55f * this->scale;
	float moveHeight2 = 1.05f * this->scale;
	float moveWidth1 = 0.45f * this->scale;
	float moveWidth2 = 0.9f * this->scale;


	this->seat->referenceFrame = this->referenceFrame;
	this->frameStack.setBaseFrame(this->seat->referenceFrame);
	this->seat->render();
	this->frameStack.push();
	{
		this->frameStack.translate(-moveWidth1, -moveHeight1, -moveWidth1);
		this->topLeftLeg->referenceFrame = this->frameStack.top();
		this->topLeftLeg->render();
		this->frameStack.push();
		{
			this->frameStack.translate(moveWidth2, 0, 0);
			this->topRightLeg->referenceFrame = this->frameStack.top();
			this->topRightLeg->render();
			this->frameStack.push();
			{
				this->frameStack.translate(0.0f, 0, moveWidth2);
				this->bottomRightLeg->referenceFrame = this->frameStack.top();
				this->bottomRightLeg->render();
				this->frameStack.push();
				{
					this->frameStack.translate(-moveWidth2, 0, 0.0f);
					this->bottomLeftLeg->referenceFrame = this->frameStack.top();
					this->bottomLeftLeg->render();
					this->frameStack.push();
					{
						this->frameStack.translate(moveWidth1, moveHeight2, 0.0);
						this->frameStack.rotateX(90);
						this->back->referenceFrame = this->frameStack.top();
						this->back->render();
					}
					this->frameStack.pop();
				}
				this->frameStack.pop();
			}
			this->frameStack.pop();
		}
		this->frameStack.pop();
	}
	this->frameStack.pop();
}
